#!/bin/bash

set -e
set -o errexit
set -o pipefail
set -o nounset

./manage.py runserver 0.0.0.0:8000 --settings=monoku_rest.test_settings
