from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Researchers(models.Model):
    id_convocatoria = models.IntegerField(_("ID CONVOCATORIA"))
    nme_convocatoria = models.CharField(_("NME CONVOCATORIA"), max_length=100)
    ano_convo = models.DateTimeField(_("ANO CONVO"))
    id_area_con_pr = models.CharField(_("ID AREA CON PR"), max_length=50)
    nme_esp_area_pr = models.CharField(_("NME ESP AREA PR"), max_length=500)
    nme_area_pr = models.CharField(_("NME AREA PR"), max_length=500)
    nme_gran_area_pr = models.CharField(_("NME_GRAN_AREA_PR"), max_length=500)
    fnacimiento_pr = models.DateTimeField(_("FNACIMIENTO_PR"))
    id_genero_pr = models.CharField(_("ID_GENERO_PR"), max_length=50)
    nme_genero_pr = models.CharField(_("NME_GENERO_PR"), max_length=50)
    nme_municipio_nac_pr = models.CharField(
        _("NME_MUNICIPIO_NAC_PR"), max_length=500)
    nme_departamento_nac_pr = models.CharField(
        _("NME_DEPARTAMENTO_NAC_PR"), max_length=500)
    nme_pais_nac_pr = models.CharField(_("NME_PAIS_NAC_PR"), max_length=500)
    nme_region_nac_pr = models.CharField(
        _("NME_REGION_NAC_PR"), max_length=500)
    cod_dane_nac_pr = models.IntegerField(_("COD_DANE_NAC_PR"))
    id_ubic_res_pr = models.CharField(_("ID_UBIC_RES_PR"), max_length=500)
    nme_municipio_res_pr = models.CharField(
        _("NME_MUNICIPIO_RES_PR"), max_length=500)
    nme_departamento_res_pr = models.CharField(
        _("NME_DEPARTAMENTO_RES_PR"), max_length=500)
    nme_pais_res_pr = models.CharField(_("NME_PAIS_RES_PR"), max_length=500)
    nme_region_res_pr = models.CharField(
        _("NME_REGION_RES_PR"), max_length=500)
    cod_dane_res_pr = models.IntegerField(_("COD_DANE_RES_PR"), default=0)
    id_niv_formacion_pr = models.CharField(
        _("ID_NIV_FORMACION_PR"), max_length=500)
    nme_niv_formacion_pr = models.CharField(
        _("NME_NIV_FORMACION_PR"), max_length=500)
    nro_orden_form_pr = models.IntegerField(
        _("NRO_ORDEN_FORM_PR"))
    id_clas_pr = models.CharField(_("ID_CLAS_PR"), max_length=500)
    nme_clasificacion_pr = models.CharField(
        _("NME_CLASIFICACION_PR"), max_length=500)
    orden_clas_pr = models.IntegerField(_("ORDEN_CLAS_PR"))
    edad_anos_pr = models.IntegerField(_("EDAD_ANOS_PR"))

    created_at = models.DateTimeField(
        _('created at'),
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    updated_at = models.DateTimeField(
        _('updated at'),
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    def __str__(self):
        return self.nme_convocatoria
