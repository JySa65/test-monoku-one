# Generated by Django 2.2.7 on 2020-03-05 16:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Researchers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_convocatoria', models.IntegerField(verbose_name='ID CONVOCATORIA')),
                ('nme_convocatoria', models.CharField(max_length=100, verbose_name='NME CONVOCATORIA')),
                ('ano_convo', models.DateTimeField(verbose_name='ANO CONVO')),
                ('id_area_con_pr', models.CharField(max_length=50, verbose_name='ID AREA CON PR')),
                ('nme_esp_area_pr', models.CharField(max_length=500, verbose_name='NME ESP AREA PR')),
                ('nme_area_pr', models.CharField(max_length=500, verbose_name='NME AREA PR')),
                ('nme_gran_area_pr', models.CharField(max_length=50, verbose_name='NME_GRAN_AREA_PR')),
                ('fnacimiento_pr', models.CharField(max_length=50, verbose_name='FNACIMIENTO_PR')),
                ('id_genero_pr', models.CharField(max_length=50, verbose_name='ID_GENERO_PR')),
                ('nme_genero_pr', models.CharField(max_length=50, verbose_name='NME_GENERO_PR')),
                ('nme_municipio_nac_pr', models.CharField(max_length=50, verbose_name='NME_MUNICIPIO_NAC_PR')),
                ('nme_departamento_nac_pr', models.CharField(max_length=50, verbose_name='NME_DEPARTAMENTO_NAC_PR')),
                ('nme_pais_nac_pr', models.CharField(max_length=50, verbose_name='NME_PAIS_NAC_PR')),
                ('nme_region_nac_pr', models.CharField(max_length=50, verbose_name='NME_REGION_NAC_PR')),
                ('cod_dane_nac_pr', models.CharField(max_length=50, verbose_name='COD_DANE_NAC_PR')),
                ('id_ubic_res_pr', models.CharField(max_length=50, verbose_name='ID_UBIC_RES_PR')),
                ('nme_municipio_res_pr', models.CharField(max_length=50, verbose_name='NME_MUNICIPIO_RES_PR')),
                ('nme_pais_res_pr', models.CharField(max_length=50, verbose_name='NME_DEPARTAMENTO_RES_PR')),
                ('nme_region_res_pr', models.CharField(max_length=50, verbose_name='NME_PAIS_RES_PR')),
                ('cod_dane_res_pr', models.CharField(max_length=50, verbose_name='COD_DANE_RES_PR')),
                ('id_niv_formacion_pr', models.CharField(max_length=50, verbose_name='ID_NIV_FORMACION_PR')),
                ('nme_niv_formacion_pr', models.CharField(max_length=50, verbose_name='NME_NIV_FORMACION_PR')),
                ('nro_orden_form_pr', models.CharField(max_length=50, verbose_name='NRO_ORDEN_FORM_PR')),
                ('id_clas_pr', models.CharField(max_length=50, verbose_name='ID_CLAS_PR')),
                ('nme_clasificacion_pr', models.CharField(max_length=50, verbose_name='NME_CLASIFICACION_PR')),
                ('orden_clas_pr', models.CharField(max_length=50, verbose_name='ORDEN_CLAS_PR')),
                ('edad_anos_pr', models.CharField(max_length=50, verbose_name='EDAD_ANOS_PR')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='updated at')),
            ],
        ),
    ]
