import openpyxl

from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings

# import django_filters.rest_framework
# from django.contrib.auth.models import User
# from myapp.serializers import UserSerializer
# from rest_framework import generics

# import Models Researchers
from app.models import Researchers


class UploadFile(View):

    def get(self, *args, **kwargs):
        path = f"{settings.BASE_DIR}/file.xlsx"
        wb = openpyxl.load_workbook(path)
        sheet = wb.active
        data = [
            Researchers(**{sheet.cell(
                row=1, column=key+1).value.lower(): cell.value for key,
                cell in enumerate(row)})
            for row in sheet.iter_rows(
                min_row=2, min_col=1, max_row=sheet.max_row, max_col=sheet.max_column)]
        Researchers.objects.bulk_create(data)
        return HttpResponse('Si')


# class UserListView(generics.ListAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#     filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
