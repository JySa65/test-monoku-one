from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from app.models.accounts import User
from app.models.researchers import Researchers


# Register your models here.
@admin.register(User)
class UserAdmin(BaseUserAdmin):
    list_display = ('email', 'first_name', 'last_name', 'created_at')
    fieldsets = (
        (None, {'fields': (
            'email',
            ('first_name',
             'last_name'),
            'password'
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions', )}),
        (_('Important dates'), {'fields': (('last_login', 'date_joined'),)}),
        ('Metadata', {
            'fields': (('created_at', 'updated_at'),),
        })
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2'
            ),
        }),
    )

    list_filter = ('is_staff', 'is_superuser', 'is_active',)
    search_fields = ('email',)
    readonly_fields = ('created_at', 'updated_at')
    ordering = ('-created_at',)


admin.site.register(Researchers)
