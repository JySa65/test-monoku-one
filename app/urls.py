from django.urls import path

from app.views import UploadFile

app_name = "app"

urlpatterns = [
    path('', UploadFile.as_view(), name="upload_file")
]
