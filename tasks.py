import os
from invoke import task, env, run
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

# env vars
env.pwd = run('pwd')
env.pwd = env.pwd.stdout.split('\n')[0]

# Tasks


@task
def init(ctx, cmd='dev'):
    path = f'cp {env.pwd}/compose/docker-compose.{cmd}.yml {env.pwd}/docker-compose.yml'
    ctx.run(path)
    print('Success')


@task
def build(ctx):
    print('In Progress...')
    name = os.getenv("NAME_PROJECT")
    name_type = os.getenv("NAME_TYPE")
    ctx.run(f'docker build --rm -t {name}/{name_type} .')


@task
def up(ctx, args=''):
    ctx.run(f'docker-compose up {args}')


@task
def down(ctx):
    ctx.run('docker-compose down')


@task
def command(ctx, cmd='shell_plus', container="monoku"):
    """
        $ inv command --cmd='shell_plus'
        $ inv command --cmd='createsuperuser'
        $ inv command --cmd='makemigrations'
    """
    ctx.run(
        f"""
            docker-compose run --rm {container} python manage.py {cmd}
        """, pty=True
    )


@task
def test(ctx, app=""):
    command(ctx, cmd=f"test {app} --settings=monoku_rest.test_settings")


@task
def createapp(ctx, app):
    command(ctx, cmd=f"startapp {app}")


@task
def migrate(ctx):
    command(ctx, cmd="migrate")


@task
def upcontainer(ctx, container="monoku"):
    "up container when there is error"
    ctx.run(f'docker-compose run --rm --service-ports {container}')


@task
def downcontainer(ctx, container):
    "docker ps and get ID and after run this command"
    ctx.run(f'docker rm -f {container}')
