FROM python:3.8
ENV PYTHONUNBUFFERED 1
RUN mkdir /monokuv
WORKDIR /monokuv
ADD requirements.txt /monokuv/
RUN pip install -r requirements.txt
ADD . /monokuv/
